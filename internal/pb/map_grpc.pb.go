// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.21.12
// source: internal/proto-files/map.proto

package pb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	Map_GetMap_FullMethodName    = "/pb.Map/GetMap"
	Map_GetMaps_FullMethodName   = "/pb.Map/GetMaps"
	Map_AddMap_FullMethodName    = "/pb.Map/AddMap"
	Map_UpdateMap_FullMethodName = "/pb.Map/UpdateMap"
	Map_DeleteMap_FullMethodName = "/pb.Map/DeleteMap"
)

// MapClient is the client API for Map service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type MapClient interface {
	GetMap(ctx context.Context, in *MapRequest, opts ...grpc.CallOption) (*MapResponse, error)
	GetMaps(ctx context.Context, in *MapRequest, opts ...grpc.CallOption) (*MapResponse, error)
	AddMap(ctx context.Context, in *MapRequest, opts ...grpc.CallOption) (*AddMapResponse, error)
	UpdateMap(ctx context.Context, in *MapRequest, opts ...grpc.CallOption) (*UpdateMapResponse, error)
	DeleteMap(ctx context.Context, in *MapRequest, opts ...grpc.CallOption) (*DeleteMapResponse, error)
}

type mapClient struct {
	cc grpc.ClientConnInterface
}

func NewMapClient(cc grpc.ClientConnInterface) MapClient {
	return &mapClient{cc}
}

func (c *mapClient) GetMap(ctx context.Context, in *MapRequest, opts ...grpc.CallOption) (*MapResponse, error) {
	out := new(MapResponse)
	err := c.cc.Invoke(ctx, Map_GetMap_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mapClient) GetMaps(ctx context.Context, in *MapRequest, opts ...grpc.CallOption) (*MapResponse, error) {
	out := new(MapResponse)
	err := c.cc.Invoke(ctx, Map_GetMaps_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mapClient) AddMap(ctx context.Context, in *MapRequest, opts ...grpc.CallOption) (*AddMapResponse, error) {
	out := new(AddMapResponse)
	err := c.cc.Invoke(ctx, Map_AddMap_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mapClient) UpdateMap(ctx context.Context, in *MapRequest, opts ...grpc.CallOption) (*UpdateMapResponse, error) {
	out := new(UpdateMapResponse)
	err := c.cc.Invoke(ctx, Map_UpdateMap_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *mapClient) DeleteMap(ctx context.Context, in *MapRequest, opts ...grpc.CallOption) (*DeleteMapResponse, error) {
	out := new(DeleteMapResponse)
	err := c.cc.Invoke(ctx, Map_DeleteMap_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// MapServer is the server API for Map service.
// All implementations must embed UnimplementedMapServer
// for forward compatibility
type MapServer interface {
	GetMap(context.Context, *MapRequest) (*MapResponse, error)
	GetMaps(context.Context, *MapRequest) (*MapResponse, error)
	AddMap(context.Context, *MapRequest) (*AddMapResponse, error)
	UpdateMap(context.Context, *MapRequest) (*UpdateMapResponse, error)
	DeleteMap(context.Context, *MapRequest) (*DeleteMapResponse, error)
	mustEmbedUnimplementedMapServer()
}

// UnimplementedMapServer must be embedded to have forward compatible implementations.
type UnimplementedMapServer struct {
}

func (UnimplementedMapServer) GetMap(context.Context, *MapRequest) (*MapResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMap not implemented")
}
func (UnimplementedMapServer) GetMaps(context.Context, *MapRequest) (*MapResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMaps not implemented")
}
func (UnimplementedMapServer) AddMap(context.Context, *MapRequest) (*AddMapResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddMap not implemented")
}
func (UnimplementedMapServer) UpdateMap(context.Context, *MapRequest) (*UpdateMapResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateMap not implemented")
}
func (UnimplementedMapServer) DeleteMap(context.Context, *MapRequest) (*DeleteMapResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteMap not implemented")
}
func (UnimplementedMapServer) mustEmbedUnimplementedMapServer() {}

// UnsafeMapServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to MapServer will
// result in compilation errors.
type UnsafeMapServer interface {
	mustEmbedUnimplementedMapServer()
}

func RegisterMapServer(s grpc.ServiceRegistrar, srv MapServer) {
	s.RegisterService(&Map_ServiceDesc, srv)
}

func _Map_GetMap_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(MapRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MapServer).GetMap(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Map_GetMap_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MapServer).GetMap(ctx, req.(*MapRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Map_GetMaps_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(MapRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MapServer).GetMaps(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Map_GetMaps_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MapServer).GetMaps(ctx, req.(*MapRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Map_AddMap_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(MapRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MapServer).AddMap(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Map_AddMap_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MapServer).AddMap(ctx, req.(*MapRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Map_UpdateMap_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(MapRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MapServer).UpdateMap(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Map_UpdateMap_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MapServer).UpdateMap(ctx, req.(*MapRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Map_DeleteMap_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(MapRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(MapServer).DeleteMap(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Map_DeleteMap_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(MapServer).DeleteMap(ctx, req.(*MapRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// Map_ServiceDesc is the grpc.ServiceDesc for Map service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Map_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "pb.Map",
	HandlerType: (*MapServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetMap",
			Handler:    _Map_GetMap_Handler,
		},
		{
			MethodName: "GetMaps",
			Handler:    _Map_GetMaps_Handler,
		},
		{
			MethodName: "AddMap",
			Handler:    _Map_AddMap_Handler,
		},
		{
			MethodName: "UpdateMap",
			Handler:    _Map_UpdateMap_Handler,
		},
		{
			MethodName: "DeleteMap",
			Handler:    _Map_DeleteMap_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "internal/proto-files/map.proto",
}
