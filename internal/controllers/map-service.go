package controllers

import (
	"context"
	"log"
	"map-grpc/internal/pb"
	"strconv"
)

type MapServiceController struct {
	pb.UnsafeMapServer
}

// NewMapServiceController returns the pointer to the implementation.
func NewMapServiceController() *MapServiceController {
	return &MapServiceController{}
}

func (serviceImpl *MapServiceController) UpdateMap(ctx context.Context, request *pb.MapRequest) (*pb.UpdateMapResponse, error) {
	//TODO implement me
	panic("implement me")
}

func (serviceImpl *MapServiceController) GetMap(ctx context.Context, request *pb.MapRequest) (*pb.MapResponse, error) {
	//TODO implement me
	panic("implement me")
}

func (serviceImpl *MapServiceController) GetMaps(ctx context.Context, request *pb.MapRequest) (*pb.MapResponse, error) {
	//TODO implement me
	panic("implement me")
}

func (serviceImpl *MapServiceController) AddMap(ctx context.Context, req *pb.MapRequest) (*pb.AddMapResponse, error) {
	log.Println("Received request for addMap repository with id " + strconv.FormatInt(int64(req.Id), 10))

	//Logic to persist to database or storage.
	log.Println("Repository persisted to the storage")

	return &pb.AddMapResponse{
		Id: req.Id,
	}, nil
}

func (serviceImpl *MapServiceController) DeleteMap(ctx context.Context, request *pb.MapRequest) (*pb.DeleteMapResponse, error) {
	return &pb.DeleteMapResponse{Status: "success"}, nil

}
