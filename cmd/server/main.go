package main

import (
	"google.golang.org/grpc"
	"log"
	"map-grpc/internal/controllers"
	"map-grpc/internal/pb"
	"net"
)

func main() {
	grpcServer := grpc.NewServer()
	server := controllers.NewMapServiceController()
	pb.RegisterMapServer(grpcServer, server) //ใน server ใส่ interface (Receiver function) ที่มีให้ครบ

	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal("failed to start")
	}

	if err := grpcServer.Serve(listener); err != nil {
		log.Fatal("failed to serve")
	}
}
